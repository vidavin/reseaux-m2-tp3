#include <stdio.h>
#include <string.h>
#include <pcap.h>
#include <netinet/ip.h>

int GLOBALE = 0;
char * device;
pcap_t * handle;
bpf_u_int32 ip_raw;
bpf_u_int32 subnet_mask_raw;

char * getPeri();
void stopEcoute();
void applicationFiltre();
void getInfoFromDevice();
void callback(u_char * user, const struct pcap_pkthdr * h, const u_char * buffer);


void callback(u_char * user, const struct pcap_pkthdr * h, const u_char * buffer)
{
    int debutEnTete = 0;

    struct ip * ipHeader;
    ipHeader = (struct ip *)(buffer+16);

//    printf("Paquet capturé de taille : %d\n", h->caplen);
//    printf("Adresse IP destination : %s\n", inet_ntoa(ipHeader->ip_dst));
// On ne veut afficher que les paquets de plus de 200 octets.

    if(h->caplen >= 200)
    {
        for(int i = 0; i < h->caplen-4; i++)
        {
            if(buffer[i] == 0x48 && buffer[i+1] == 0x54 && buffer[i+2] == 0x54 && buffer[i+3] == 0x50)
            {
                debutEnTete = i;
                break;
            }
        }
        
        if(buffer[debutEnTete+9] == 0x34 && buffer[debutEnTete+10] == 0x30 && buffer[debutEnTete+11] == 0x34)
        {
            GLOBALE ++;
            if(GLOBALE >= 2)
            {
                GLOBALE = 0;
                printf("\n\n /!\\ Erreur : 2 obtentions de réponse 404\n\n");
            }
        }
        
        printf("On a capturé un paquet de plus de 200 octets\n");
        for(int i = debutEnTete; i < h->caplen; i++)
        {
            printf("%c", buffer[i]);
        }
        
    }
}


char * getPeri()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    device = pcap_lookupdev(errbuf);
    if(device == NULL)
    {
        printf("Erreur récupération du périphérique : \n");
        printf("%s\n", errbuf);
    }
    else
    {
        printf("Device : %s\n", device);
    }
    return device;
}


void getEcoute()
{
    getPeri();
    char errbuf[PCAP_ERRBUF_SIZE];
    handle = pcap_open_live(device, 1024, 0, -1, errbuf);

}


void stopEcoute()
{
    pcap_close(handle);
}


void getInfoFromDevice()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    char ip[16];
    char subnet_mask[16];

    pcap_lookupnet(device, &ip_raw, &subnet_mask_raw, errbuf);


    struct in_addr address;
    address.s_addr = ip_raw;
    strcpy(ip, inet_ntoa(address));

    address.s_addr = subnet_mask_raw;
    strcpy(subnet_mask, inet_ntoa(address));

    printf("Ip address : %s\n", ip);
    printf("Subnet mask : %s\n", subnet_mask);
}


void applicationFiltre()
{   
    struct bpf_program filter;    
    char filter_exp[] = "port 80";


    if(pcap_compile(handle, &filter, filter_exp, 0, ip_raw) == -1)
        printf("Mauvais filtre - %s\n", pcap_geterr(handle));
    

    if(pcap_setfilter(handle, &filter) == -1)
        printf("Erreur lancement filtre - %s\n", pcap_geterr(handle));

}


void main()
{
    getEcoute();
    getInfoFromDevice();
    applicationFiltre();

    pcap_loop(handle, -1, &callback, NULL);


    stopEcoute();
}

